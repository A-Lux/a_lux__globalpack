$(document).ready(function(){
    var owl = $('.owl-carousel-production');
    $(owl).owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        autoplay:true,
        autoplayTimeout:2000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            900:{
                items:3
            },
            1324:{
                items:4
            }
        }
    });
    $('.next').click(function(e) {
        e.preventDefault();
        owl.trigger('next.owl.carousel');
    });
    $('.prev').click(function(e) {
        e.preventDefault();
        owl.trigger('prev.owl.carousel');
    });
    
    var owlGoods = $('.owl-carousel-goods');
    $(owlGoods).owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        autoplay:true,
        autoplayTimeout:2000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });



    $('.menu-catalog').click(function(){
        $('.menu-catalog-content').toggle(300);
        $('.burger-menu-lines').toggleClass('burger-menu-lines-active');
    });


    $('.burger-mobile').click(function(){
        $('.burger-mobile-lines').toggleClass('burger-mobile-lines-active');
        $('.mobile-nav').toggleClass('mobile-nav-active')
        $("body").toggleClass("body-overflow");
        $('.mobile-nav ul li').toggleClass('li-active')
    });
    $('.carousel').carousel({
        interval: false
    }); 

    $("#phone").mask("+7(999) 999-9999");
});