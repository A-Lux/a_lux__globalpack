$(document).ready(function () {
    var owlCarouselModal = $('.owl-carousel-modal');
    $(owlCarouselModal).owlCarousel({
        loop: true,
        dots: false,
        margin: 10,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    var owlCarouselGalery = $('.owl-carousel-galery');
    $(owlCarouselGalery).owlCarousel({
        loop: false,
        dots: false,
        loop: true,
        margin: 10,
        nav: false,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });
    $('.galery-next').click(function (q) {
        q.preventDefault();
        owlCarouselModal.trigger('next.owl.carousel');
    });
    $('.galery-prev').click(function (q) {
        q.preventDefault();
        owlCarouselModal.trigger('prev.owl.carousel');
    });

    $('.item-galery img').click(function () {
        var attr = $(this).attr('src');
        $('.galery-main img').attr('src', attr);
        console.log($(this).attr('data-galery'));
    });
    // quantity

    $('.plus').click(function () {
        ++document.getElementById('quantity').value;
    });
    $('.minus').click(function () {
        --document.getElementById('quantity').value;
    });

    // CATALOG-TAB

    $('.tab-head li').click(function () {
        var tabAttr = $(this).attr('data-tab');
        console.log($('.modal-tab ul li'));

        $('.tab-head li').removeClass('tab-active');
        $(this).addClass('tab-active');

        $('.modal-description').removeClass('description-active');
        $('#' + tabAttr).addClass('description-active');

    });

    $('.shoping-img').click(function () {
        $('.shoping-fix').toggleClass('shoping-fix-active');
        $('.shoping-dark').toggleClass('shoping-active');
    });
    $(document).mouseup(function (e) {
        let catalog = $('.shoping-fix');
        if (!catalog.is(e.target) && catalog.has(e.target).length === 0) {
            catalog.removeClass('shoping-fix-active');
            $('.shoping-dark').removeClass('shoping-active');
        }
    });

    $('.rangeV').on('input', function () {
        var val = $(this).val();
        $(this).css({ 'background': '-webkit-linear-gradient(left, #e42a1e 0%,#e42a1e ' + val + '%, #fff ' + val + '%, #fff 100%)' });
    });
});