<?php include 'header.php'; ?>
<?php include 'shoping-fix.php'; ?>

<div class="container">
    <div class="page">
        <ul class="p-0 m-0">
            <li><a href="#">Главная</a></li>
            <li><img src="images/page-next.png" alt=""></li>
            <li><a href="#">О нас</a></li>
        </ul>
    </div>
    <div class="title">
        <h1>Каталог</h1>
    </div>
</div>

<!-- CATALOG -->
<div class="catalog">
    <div class="catalog-filter">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-md-6 d-flex align-items-center">
                    <h4>Подберите продукцию по фильтрам</h4>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="volume">
                        <h5>Объем</h5>
                        <div class="value-inp">
                            <input type="text" value="30">
                            <img src="images/inp-figure.png" alt="">
                            <input type="text" value="1500">
                        </div>
                        <input type="range" name="" class="rangeV" min="0" max="100" value="50" multiple>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="filter-product">
                        <h5>Продукция</h5>
                        <div class="filter-checkbox">
                            <div class="row">
                                <div class="col-xl-3 col-md-4 pr-0 d-flex pb-2">
                                    <input type="checkbox" name="" id=""><label for="">Под кофе</label>
                                </div>
                                <div class="col-xl-3 col-md-4 p-0 d-flex pb-2">
                                    <input type="checkbox" name="" id=""><label for="">Под горячее</label>
                                </div>
                                <div class="col-xl-4 col-md-4 p-0 d-flex pb-2">
                                    <input type="checkbox" name="" id=""><label for="">Для салатов</label>
                                </div>
                                <div class="col-xl-3 col-md-4 pr-0 d-flex pb-2">
                                    <input type="checkbox" name="" id=""><label for="">Под торты</label>
                                </div>
                                <div class="col-xl-3 col-md-4 p-0 d-flex pb-2">
                                    <input type="checkbox" name="" id=""><label for="">Под холодное</label>
                                </div>
                                <div class="col-xl-4 col-md-4 p-0 d-flex pb-2">
                                    <input type="checkbox" name="" id=""><label for="">Для кондитерских изд. </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="catalog-product">
            <div class="row">
                <div class="col-xl-3 col-md-4  mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <p class="quick-view" data-toggle="modal" data-target="#exampleModal">Быстрый просмотр</p>
                            <img src="images/production-1.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>400 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global" data-toggle="modal" data-target="#buy">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4  mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-2.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан вендинговый 150 мл. (180 мл. до края), d-70, БЕЛЫЙ</p>
                            </div>
                            <div class="product-price">
                                <h5>430 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4  mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-3.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>410 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4  mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-4.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>450 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4  mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-1.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>400 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4 mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-2.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>430 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4  mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-3.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>410 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4 mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-4.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>450 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4 mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-1.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>400 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4 mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-2.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>430 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4 mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-3.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>410 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-4 mb-4">
                    <div class="product-content">
                        <div class="product-img text-center">
                            <img src="images/production-4.png" alt="">
                        </div>
                        <div class="product-body">
                            <div class="product-text">
                                <p class="small">Артикул: 4526587</p>
                                <p>Стакан 100 мл. (120 мл. до края), белый (БУМ, Formacia)</p>
                            </div>
                            <div class="product-price">
                                <h5>450 тенге</h5>
                            </div>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Button trigger modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="modal-slider">
                            <div class="owl-carousel owl-carousel-modal owl-theme">
                                <div class="item galery-main">
                                    <img src="images/modal-owl.png" alt="">
                                </div>
                                <div class="item galery-main">
                                    <img src="images/modal-owl.png" alt="">
                                </div>
                                <div class="item galery-main">
                                    <img src="images/modal-owl.png" alt="">
                                </div>
                                <div class="item galery-main">
                                    <img src="images/modal-owl.png" alt="">
                                </div>
                            </div>
                            <a href="#" class="galery-prev"><img src="images/galery-prev.png" alt=""></a>
                            <a href="#" class="galery-next"><img src="images/galery-next.png" alt=""></a>
                        </div>

                        <div class="modal-galery">
                            <div class="owl-carousel owl-carousel-galery owl-theme">
                                <div class="item item-galery">
                                    <img src="images/modal-galery-1.png" alt="" data-galery="galery-item-1">
                                </div>
                                <div class="item item-galery">
                                    <img src="images/modal-galery-2.png" alt="" data-galery="galery-item-2">
                                </div>
                                <div class="item item-galery">
                                    <img src="images/modal-galery-3.png" alt="" data-galery="galery-item-3">
                                </div>
                                <div class="item item-galery">
                                    <img src="images/modal-galery-4.png" alt="" data-galery="galery-item-4">
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <h4>Что входит в комплект</h4>
                        <div class="modal-galery">
                            <div class="owl-carousel owl-carousel-galery owl-theme">
                                <div class="item-cont">
                                    <img src="images/cont.png" alt="">
                                    <p>Крышка
                                        к контейнеру</p>
                                </div>
                                <div class="item-cont">
                                    <img src="images/cont.png" alt="">
                                    <p>Крышка
                                        к контейнеру</p>
                                </div>
                                <div class="item-cont">
                                    <img src="images/cont.png" alt="">
                                    <p>Крышка
                                        к контейнеру</p>
                                </div>
                                <div class="item-cont">
                                    <img src="images/cont.png" alt="">
                                    <p>Крышка
                                        к контейнеру</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="right-modal-content">
                            <div class="small">Артикул: 4526587</div>
                            <h4>Стакан 100 мл. (120 мл. до края),с нанесением (бум, formacia)</h4>
                            <br>
                            <div class="modal-tab">
                                <ul class="p-0 d-flex tab-head">
                                    <li class="tab-active" data-tab="modal-tab-1">Описание</li>
                                    <li data-tab="modal-tab-2" data-tab="modal-tab-2">Характеристики</li>
                                </ul>
                                <br>
                                <div class="modal-description description-active" id="modal-tab-1">
                                    <p>Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности требуют от нас анализа модели развития. С другой стороны постоянный </p>
                                </div>
                                <div class="modal-description" id="modal-tab-2">
                                    <ul class="p-0">
                                        <li>Материал изготовления:<span class="font-weight-bold">Полипропилен</span></li>
                                        <li>Подходит для горячих продуктов:<span class="font-weight-bold">да</span></li>
                                        <li>Диаметр крышки (мм):<span class="font-weight-bold">130</span></li>
                                        <li>Новинка:<span class="font-weight-bold">да</span></li>
                                        <li>Цвет:<span class="font-weight-bold">Прозрачный</span></li>
                                        <li>Производитель:<span class="font-weight-bold">СтиролПласт</span></li>
                                        <li>Товары для:<span class="font-weight-bold">супов, горячих блюд</span></li>
                                    </ul>
                                </div>
                            </div>
                            <br><br>
                            <div class="product-price">
                                <h4>400 тенге</h4>
                            </div>
                            <br>
                            <div class="quantity">
                                <p>Количество</p>
                                <form action="">
                                    <span class="plus"><img src="images/plus.png" alt=""></span>
                                    <input type="text" value="500" id="quantity" disabled>
                                    <span class="minus"><img src="images/minus.png" alt=""></span>
                                </form>
                                <b>шт.</b>
                            </div>
                            <br>
                            <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                            <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal modal-buy fade" id="buy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="small">Артикул: 4526587</p>
                    <h3><span class="red-text">Купить </span>стакан 100 мл. (120 мл. до края),с нанесением (бум, formacia)</h3>
                    <br><br>

                    <!-- input -->
                    <form action="">
                        <label for="">Введите ваше ФИО</label>
                        <input type="text" placeholder="Иванов Александр Сергеевич">
                        <br><br>
                        <label for="">Введите ваш ИНН</label>
                        <input type="text" placeholder="78955468">
                        <br><br>
                        <label for="">Введите ваш телефон</label>
                        <input type="text" placeholder="+7(___)" id="phone">
                    </form>
                    <br>
                    <div class="quantity">
                        <p>Количество</p>
                        <form action="">
                            <span class="plus"><img src="images/plus.png" alt=""></span>
                            <input type="text" value="500" id="quantity" disabled>
                            <span class="minus"><img src="images/minus.png" alt=""></span>
                        </form>
                        <b>шт.</b>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger btn-global" data-dismiss="modal">Купить товар</button>
                </div>
            </div>
        </div>
    </div>
</div>





<?php include 'footer.php'; ?>