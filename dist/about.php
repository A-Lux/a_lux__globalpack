<?php include 'header.php'; ?>

<div class="about">
    <div class="container p-0">
        <div class="page">
            <ul class="p-0 m-0">
                <li><a href="#">Главная</a></li>
                <li><img src="images/page-next.png" alt=""></li>
                <li><a href="#">О нас</a></li>
            </ul>
        </div>
        <div class="title">
            <h1>О нас</h1>
        </div>
        <div class="about-text">
            <h3>Производим продукцию от малых до больших заказов</h3>
            <br>
            <p>
                Идейные соображения высшего порядка, а также консультация с широким активом требуют от нас анализа направлений прогрессивного развития. Таким образом укрепление и развитие структуры влечет за собой процесс внедрения и модернизации позиций, занимаемых участниками в отношении поставленных задач. Не следует, однако забывать, что сложившаяся структура организации представляет собой интересный эксперимент проверки существенных финансовых и административных условий. Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности способствует подготовки и реализации
            </p>
        </div>
        <br>
        <div class="about-production">
            <h3>Посмотрите на производство изнутри</h3>
            <br>
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <img src="images/about-product-1.png" alt="">
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="row">
                        <div class="col-xl-6 col-md-6">
                            <img src="images/about-product-2.png" alt="">
                        </div>
                        <div class="col-xl-6 col-md-6">
                            <img src="images/about-product-3.png" alt="">
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <br>
                            <div class="leave-request">
                                <h3><span>Есть вопросы?</span> Оставьте заявку</h3>
                                <p>И мы ответим на ваши вопросы</p>
                                <br><br>
                                <button class="btn btn-outline-danger btn-global">Оставить заявку</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
            <p>
                Идейные соображения высшего порядка, а также консультация с широким активом требуют от нас анализа направлений прогрессивного развития. Таким образом укрепление и развитие структуры влечет за собой процесс внедрения и модернизации позиций, занимаемых участниками в отношении поставленных задач. Не следует, однако забывать, что сложившаяся структура организации представляет собой интересный эксперимент проверки существенных финансовых и административных условий. Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности способствует подготовки и реализации
            </p>
            <br><br>
        </div>
    </div>
    <div class="advantages">
        <div class="container">
            <h3>Наши преимущества</h3>
            <div class="col-xl-7 p-0">
                <p>Идейные соображения высшего порядка, а также консультация с широким активом требуют от нас анализа направлений прогрессивного развития. Таким образом укрепление и развитие структуры влечет за собой процесс внедрения и модернизации</p>
                <br>
                <ul class="p-0">
                    <li>
                        <img src="images/verified.png" alt="">
                        Первое преимущество
                    </li>
                    <li>
                        <img src="images/verified.png" alt="">
                        Второе преимущество
                    </li>
                    <li>
                        <img src="images/verified.png" alt="">
                        Третье преимущество
                    </li>
                    <li>
                        <img src="images/verified.png" alt="">
                        Четвертое преимущество
                    </li>
                    <li>
                        <img src="images/verified.png" alt="">
                        Пятое преимущество
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <!-- GLOBAL-INFO -->
        <div class="global-info">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="main-info info-bg-1">
                            <img src="images/num-1.png" alt="">
                            <div class="main-info-text">
                                <h4>Работаем уже <br> с 2008 года</h4>
                                <p>
                                    Наша компания открылась
                                    <br>
                                    в декабре 2008 года
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 p-0">
                        <div class="main-info info-bg-2">
                            <img src="images/num-2.png" alt="">
                            <div class="main-info-text">
                                <h4>Надежный поставщик в Казахстане</h4>
                                <p>
                                    Большинство крупных игроков пищевой индустрии Казахстана работают с нашей компанией, так как мы зарекомендовали себя как надежного поставщика, проверенного временем
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="main-info info-bg-3">
                            <img src="images/num-3.png" alt="">
                            <div class="main-info-text">
                                <h4>Свыше <br> 300 наименований</h4>
                                <p>
                                    Ежедневно мы снабжаем упаковкой как крупных клиентов, так и тех, кто только начинает. В нашем ассортименте свыше 300 наименований продукции.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- GLOBAL-INFO-END -->
        <div class="distributor">
            <div class="Official-distributor">
                <div class="distributor-block">
                    <div class="distributor-text">
                        <h3>Официальный дистрибьютор</h3>
                        <br>
                        <p>В 2019 году наша компания стала официальным Дистрибьютором мирового лидера в сфере производства бумажной продукции и продукции из ПЭТ завода изготовителя Huhtamaki</p>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="Customer-base">
                <div class="distributor-block">
                    <div class="distributor-text">
                        <h3>Клиентская база</h3>
                        <br>
                        <p>Большинство крупных игроков пищевой индустрии Казахстана работают с нашей компанией, так как мы зарекомендовали себя как надежного поставщика, проверенного временем</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="instagram">
        <div class="container">
            <div class="col-xl-5">
                <div class="instagram-text">
                    <h3>Подписывайтесь на наш инстаграм</h3>
                    <br>
                    <p>Идейные соображения высшего порядка, а также консультация с широким активом требуют от нас анализа направлений прогрессивного развития. Таким образом укрепление и развитие структуры влечет за собой процесс внедрения и </p>
                </div>
                <br><br>
                <a href="#" class="btn btn-global inst-link"><img src="images/instagram-logo.png" alt="">/ upakovka_ast</a>
                <a href="#" class="btn btn-outline-danger btn-global"><img src="images/instagram-logo.png" alt="">Перейти в инстаграм</a>
            </div>
        </div>
    </div>

    <div class="about-text-foot text-center">
        <span>
            <img src="images/quots.png" alt="">
        </span>
        <h3>
            Упаковка важна так же, как и продукт.
            Иногда даже важнее
        </h3>
    </div>
</div>


<?php include 'footer.php'; ?>