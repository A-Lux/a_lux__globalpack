<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="css/main.css">
    <title>Document</title>
</head>

<body>
    <div class="header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-3 col-md-3">
                    <div class="logo">
                        <a href="index.php"><img src="images/header-logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 desk">
                    <div class="nav">
                        <ul class="p-0 d-flex w-100 justify-content-between">
                            <li>
                                <a href="catalog.php">Продукция</a>
                            </li>
                            <li>
                                <a href="about.php">О компании</a>
                            </li>
                            <li>
                                <a href="#">Сертификаты</a>
                            </li>
                            <li>
                                <a href="contact.php">Контакты</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-md-3 text-right desk">
                    <div class="big-text">
                        <p>+7 700 123‑60-56</p>
                    </div>
                </div>
                <div class="col-xl-3 col-md-12 col-lg-3 mt-3">
                    <div class="menu-catalog">
                        <div class="menu-button">
                            <span class="burger-menu-lines"></span>
                            <p>Каталог продукции</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-md-7 mt-3 desk">
                    <form action="">
                        <input type="text" placeholder="Поиск по продукции">
                        <a href="#"><img src="images/search-logo.png" alt=""></a>
                    </form>
                </div>
                <div class="col-xl-2 col-md-2 p-0 mt-3 desk">
                    <a href="#" type="btn" class="btn btn-outline-danger btn-global"><img src="images/wp-btn.png" alt=""> Обратный звонок</a>
                </div>
            </div>
        </div>
        <div class="mobile-basket">
            <a href="basket.php"><img src="images/shopping-bag.png" alt=""></a>
        </div>
    </div>
    <div class="container position-relative">
        <div class="menu-catalog-content">
            <!-- <span></span> -->
            <ul class="p-0">
                <li><a href="#">Ссылка-1</a></li>
                <li><a href="#">Ссылка-2</a></li>
                <li><a href="#">Ссылка-3</a></li>
                <li><a href="#">Ссылка-4</a></li>
                <li><a href="#">Ссылка-5</a></li>
                <li><a href="#">Ссылка-6</a></li>
                <li><a href="#">Ссылка-7</a></li>
                <li><a href="#">Ссылка-1</a></li>
                <li><a href="#">Ссылка-2</a></li>
                <li><a href="#">Ссылка-3</a></li>
                <li><a href="#">Ссылка-4</a></li>
                <li><a href="#">Ссылка-5</a></li>
                <li><a href="#">Ссылка-6</a></li>
                <li><a href="#">Ссылка-7</a></li>
            </ul>
        </div>
    </div>
    <div class="mobile-menu">
        <div class="burger-mobile">
            <span class="burger-mobile-lines"></span>
        </div>
    </div>
    <div class="mobile-nav">
        <ul class="p-0 text-center">
            <li>
                <a href="#">Продукция</a>
            </li>
            <li>
                <a href="#">О компании</a>
            </li>
            <li>
                <a href="#">Сертификаты</a>
            </li>
            <li>
                <a href="#">Контакты</a>
            </li>
        </ul>
    </div>