

<div class="shoping-dark"></div>
<div class="shoping-fix">
    <div class="shoping-img">
        <img src="images/shopping-bag-fix.png" alt="">
    </div>
    <div class="shoping-fix-basket">
        <h2>Корзина</h2>
        <div class="shoping-fix-content">
            <div class="col-xl-2 p-0">
                <div class="shoping-fix-img">
                    <img src="images/basket-1.png" alt="">
                </div>
            </div>
            <div class="col-xl-4">
                <div class="shoping-fix-text">
                    <p class="small">Артикул: 4526587</p>
                    <p>Стакан 100 мл. (120 мл. до края),
                        с нанесением (бум, formacia)</p>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="quantity">
                    <h5>410 тенге</h5>
                    <form action="">
                        <span class="plus"><img src="images/plus.png" alt=""></span>
                        <input type="text" value="500" id="quantity" disabled>
                        <span class="minus"><img src="images/minus.png" alt=""></span>
                    </form>
                    <h5>шт.</h5>
                </div>
            </div>
            <div class="delet-product">
                <img src="images/delet-basket.png" alt="">
            </div>
        </div>
        <hr>
        <div class="shoping-fix-content">
            <div class="col-xl-2 p-0">
                <div class="shoping-fix-img">
                    <img src="images/basket-1.png" alt="">
                </div>
            </div>
            <div class="col-xl-4">
                <div class="shoping-fix-text">
                    <p class="small">Артикул: 4526587</p>
                    <p>Стакан 100 мл. (120 мл. до края),
                        с нанесением (бум, formacia)</p>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="quantity">
                    <h5>410 тенге</h5>
                    <form action="">
                        <span class="plus"><img src="images/plus.png" alt=""></span>
                        <input type="text" value="500" id="quantity" disabled>
                        <span class="minus"><img src="images/minus.png" alt=""></span>
                    </form>
                    <h5>шт.</h5>
                </div>
            </div>
            <div class="delet-product">
                <img src="images/delet-basket.png" alt="">
            </div>
        </div>
        <hr>
        <br>
        <div class="shoping-fix-footer">
            <div class="tatal d-flex align-items-baseline">
                <h6>Итого:</h6>
                <h2>820 тенге</h2>
            </div>
            <br>
            <div class="basket-btn d-flex">
                <button class="btn btn-outline-danger btn-global"><img src="images/shopping-bag.png" alt="">В корзину</button>
                <button class="btn btn-outline-danger btn-global">Очистить корзину</button>
            </div>
        </div>
    </div>
</div>