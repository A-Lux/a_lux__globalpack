/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/catalog.js":
/*!***************************!*\
  !*** ./src/js/catalog.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("$(document).ready(function () {\r\n    var owlCarouselModal = $('.owl-carousel-modal');\r\n    $(owlCarouselModal).owlCarousel({\r\n        loop: true,\r\n        dots: false,\r\n        margin: 10,\r\n        nav: false,\r\n        responsive: {\r\n            0: {\r\n                items: 1\r\n            },\r\n            600: {\r\n                items: 1\r\n            },\r\n            1000: {\r\n                items: 1\r\n            }\r\n        }\r\n    });\r\n\r\n    var owlCarouselGalery = $('.owl-carousel-galery');\r\n    $(owlCarouselGalery).owlCarousel({\r\n        loop: false,\r\n        dots: false,\r\n        loop: true,\r\n        margin: 10,\r\n        nav: false,\r\n        responsive: {\r\n            0: {\r\n                items: 2\r\n            },\r\n            600: {\r\n                items: 3\r\n            },\r\n            1000: {\r\n                items: 4\r\n            }\r\n        }\r\n    });\r\n    $('.galery-next').click(function (q) {\r\n        q.preventDefault();\r\n        owlCarouselModal.trigger('next.owl.carousel');\r\n    });\r\n    $('.galery-prev').click(function (q) {\r\n        q.preventDefault();\r\n        owlCarouselModal.trigger('prev.owl.carousel');\r\n    });\r\n\r\n    $('.item-galery img').click(function () {\r\n        var attr = $(this).attr('src');\r\n        $('.galery-main img').attr('src', attr);\r\n        console.log($(this).attr('data-galery'));\r\n    });\r\n    // quantity\r\n\r\n    $('.plus').click(function () {\r\n        ++document.getElementById('quantity').value;\r\n    });\r\n    $('.minus').click(function () {\r\n        --document.getElementById('quantity').value;\r\n    });\r\n\r\n    // CATALOG-TAB\r\n\r\n    $('.tab-head li').click(function () {\r\n        var tabAttr = $(this).attr('data-tab');\r\n        console.log($('.modal-tab ul li'));\r\n\r\n        $('.tab-head li').removeClass('tab-active');\r\n        $(this).addClass('tab-active');\r\n\r\n        $('.modal-description').removeClass('description-active');\r\n        $('#' + tabAttr).addClass('description-active');\r\n\r\n    });\r\n\r\n    $('.shoping-img').click(function () {\r\n        $('.shoping-fix').toggleClass('shoping-fix-active');\r\n        $('.shoping-dark').toggleClass('shoping-active');\r\n    });\r\n    $(document).mouseup(function (e) {\r\n        let catalog = $('.shoping-fix');\r\n        if (!catalog.is(e.target) && catalog.has(e.target).length === 0) {\r\n            catalog.removeClass('shoping-fix-active');\r\n            $('.shoping-dark').removeClass('shoping-active');\r\n        }\r\n    });\r\n\r\n    $('.rangeV').on('input', function () {\r\n        var val = $(this).val();\r\n        $(this).css({ 'background': '-webkit-linear-gradient(left, #e42a1e 0%,#e42a1e ' + val + '%, #fff ' + val + '%, #fff 100%)' });\r\n    });\r\n});\n\n//# sourceURL=webpack:///./src/js/catalog.js?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("$(document).ready(function(){\r\n    var owl = $('.owl-carousel-production');\r\n    $(owl).owlCarousel({\r\n        loop:true,\r\n        margin:10,\r\n        nav:false,\r\n        autoplay:true,\r\n        autoplayTimeout:2000,\r\n        responsive:{\r\n            0:{\r\n                items:1\r\n            },\r\n            600:{\r\n                items:2\r\n            },\r\n            900:{\r\n                items:3\r\n            },\r\n            1324:{\r\n                items:4\r\n            }\r\n        }\r\n    });\r\n    $('.next').click(function(e) {\r\n        e.preventDefault();\r\n        owl.trigger('next.owl.carousel');\r\n    });\r\n    $('.prev').click(function(e) {\r\n        e.preventDefault();\r\n        owl.trigger('prev.owl.carousel');\r\n    });\r\n    \r\n    var owlGoods = $('.owl-carousel-goods');\r\n    $(owlGoods).owlCarousel({\r\n        loop:true,\r\n        margin:10,\r\n        nav:false,\r\n        autoplay:true,\r\n        autoplayTimeout:2000,\r\n        responsive:{\r\n            0:{\r\n                items:1\r\n            },\r\n            600:{\r\n                items:3\r\n            },\r\n            1000:{\r\n                items:4\r\n            }\r\n        }\r\n    });\r\n\r\n\r\n\r\n    $('.menu-catalog').click(function(){\r\n        $('.menu-catalog-content').toggle(300);\r\n        $('.burger-menu-lines').toggleClass('burger-menu-lines-active');\r\n    });\r\n\r\n\r\n    $('.burger-mobile').click(function(){\r\n        $('.burger-mobile-lines').toggleClass('burger-mobile-lines-active');\r\n        $('.mobile-nav').toggleClass('mobile-nav-active')\r\n        $(\"body\").toggleClass(\"body-overflow\");\r\n        $('.mobile-nav ul li').toggleClass('li-active')\r\n    });\r\n    $('.carousel').carousel({\r\n        interval: false\r\n    }); \r\n\r\n    $(\"#phone\").mask(\"+7(999) 999-9999\");\r\n});\n\n//# sourceURL=webpack:///./src/js/index.js?");

/***/ }),

/***/ 0:
/*!***************************************************!*\
  !*** multi ./src/js/index.js ./src/js/catalog.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./src/js/index.js */\"./src/js/index.js\");\nmodule.exports = __webpack_require__(/*! ./src/js/catalog.js */\"./src/js/catalog.js\");\n\n\n//# sourceURL=webpack:///multi_./src/js/index.js_./src/js/catalog.js?");

/***/ })

/******/ });