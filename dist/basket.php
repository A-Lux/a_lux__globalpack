<?php include 'header.php'; ?>
<?php include 'shoping-fix.php'; ?>

<div class="container">
    <div class="page">
        <ul class="p-0 m-0">
            <li><a href="#">Главная</a></li>
            <li><img src="images/page-next.png" alt=""></li>
            <li><a href="#">О нас</a></li>
        </ul>
    </div>
    <div class="title">
        <h1>Корзина</h1>
    </div>
</div>

<!-- Корзина -->
<div class="basket">
    <div class="container">
        <div class="basket-content">
            <div class="row">
                <div class="col-xl-2 text-center">
                    <div class="basket-img">
                        <img src="images/basket-1.png" alt="">
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="basket-text">
                        <p class="small">Артикул: 4526587</p>
                        <p>Стакан 100 мл. (120 мл. до края),
                            с нанесением (бум, formacia)</p>
                    </div>
                </div>
                <div class="col-xl-5 d-flex justify-content-end">
                    <div class="quantity">
                        <h5>410 тенге</h5>
                        <form action="">
                            <span class="plus"><img src="images/plus.png" alt=""></span>
                            <input type="text" value="500" id="quantity" disabled>
                            <span class="minus"><img src="images/minus.png" alt=""></span>
                        </form>
                        <h5>шт.</h5>
                    </div>
                </div>
                <div class="col-xl-1 d-flex align-items-center position-static">
                    <div class="delet-product">
                        <img src="images/delet-basket.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="basket-content">
            <div class="row">
                <div class="col-xl-2 text-center">
                    <div class="basket-img">
                        <img src="images/basket-2.png" alt="">
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="basket-text">
                        <p class="small">Артикул: 4526587</p>
                        <p>Стакан 100 мл. (120 мл. до края),
                            с нанесением (бум, formacia)</p>
                    </div>
                </div>
                <div class="col-xl-5 d-flex justify-content-end">
                    <div class="quantity">
                        <h5>410 тенге</h5>
                        <form action="">
                            <span class="plus"><img src="images/plus.png" alt=""></span>
                            <input type="text" value="500" id="quantity" disabled>
                            <span class="minus"><img src="images/minus.png" alt=""></span>
                        </form>
                        <h5>шт.</h5>
                    </div>
                </div>
                <div class="col-xl-1 d-flex align-items-center position-static">
                    <div class="delet-product">
                        <img src="images/delet-basket.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="total d-flex align-items-baseline">
            <h6>Итого: </h6>
            <h2> 820 тенге</h2>
        </div>
        <br>
        <button type="button" class="btn btn-outline-secondary btn-global btn-basket">Очистить корзину</button>
        <br>
    </div>

    <div class="basket-form">
        <div class="container">
            <h1>Оформление заказа</h1>
            <br>
            <div class="col-xl-6 p-0">
                <label for="">Ваше имя<span> *</span></label>
                <br>
                <input type="text" placeholder="Александр">
                <br><br>
                <label for="">Ваше имя<span> *</span></label>
                <br>
                <input type="text" placeholder="info@mail.ru">
                <br><br>
                <label for="">Ваше имя<span> *</span></label>
                <br>
                <input type="text" placeholder="+7(___)" id="phone">
                <br><br>
                <label for="">Адрес доставки<span> *</span></label>
                <br>
                <div class="row">
                    <div class="col-xl-6">
                        <select name="" id="">
                            <option value="">afpkapfkaf</option>
                            <option value="">afpkapfkaf</option>
                            <option value="">afpkapfkaf</option>
                        </select>
                    </div>
                    <div class="col-xl-6">
                        <input type="text" placeholder="Балашиха, ул. Красная 17, кв 90">
                    </div>
                </div>
                <br><br>
                <label for="">Комментарий к заказу<span> *</span></label>
                <br>
                <textarea name="" id="" cols="30" rows="5" placeholder="Напишите несколько пожеланий к заказу"></textarea>
            </div>
            <br>
            <div>
                <div class="order-pice ml-1 ">
                    <button class="btn btn-outline-secondary btn-global">Оформить заказ</button>
                    <div class="total d-flex align-items-baseline">
                        <h6>Итого: </h6>
                        <h4> 4580 тенге</h4>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'end-slider.php'; ?>





<?php include 'footer.php'; ?>