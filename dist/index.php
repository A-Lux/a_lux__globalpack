<?php include 'header.php'; ?>

<!-- SLIDER-->
<div class="main-slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/bg-slide2.png" class="d-block w-100" alt="...">
                <div class="carousel-text">
                    <h1>Упаковочная продукция</h1>
                    <h5>GLOBAL PACK KZ — универсальный поставщик
                        упаковочной продукции</h5>
                    <br>
                    <div class="carousel-btn">
                        <a href="#" class="btn btn-outline-danger btn-global">Подробнее</a>
                        <a href="#" class="btn btn-outline-danger btn-global">Перейти в инстаграм <img src="images/instagram-logo.png" alt=""></a>
                    </div>
                </div>
                <div class="carousel-link">
                    <a href="#" class="link-pos-1">Для кофе </a>
                    <a href="#" class="link-pos-2">Для напитков</a>
                    <a href="#" class="link-pos-3">Для тортов</a>
                    <a href="#" class="link-pos-4">Для выпечки</a>
                    <a href="#" class="link-pos-5">Для горячих блюд</a>
                </div>
            </div>
            <div class="carousel-item">
                <img src="images/bg-slide2.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/bg-slide2.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/bg-slide2.png" class="d-block w-100" alt="...">
            </div>
        </div>
    </div>
</div>

<!-- SLIDER-END -->

<!-- GLOBAL-INFO -->
<div class="global-info">
    <div class="container">
        <div class="row">
            <div class="col-xl-4">
                <div class="main-info info-bg-1">
                    <img src="images/num-1.png" alt="">
                    <div class="main-info-text">
                        <h4>Работаем уже <br> с 2008 года</h4>
                        <p>
                            Наша компания открылась
                            <br>
                            в декабре 2008 года
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 p-0">
                <div class="main-info info-bg-2">
                    <img src="images/num-2.png" alt="">
                    <div class="main-info-text">
                        <h4>Надежный поставщик в Казахстане</h4>
                        <p>
                            Большинство крупных игроков пищевой индустрии Казахстана работают с нашей компанией, так как мы зарекомендовали себя как надежного поставщика, проверенного временем
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="main-info info-bg-3">
                    <img src="images/num-3.png" alt="">
                    <div class="main-info-text">
                        <h4>Свыше <br> 300 наименований</h4>
                        <p>
                            Ежедневно мы снабжаем упаковкой как крупных клиентов, так и тех, кто только начинает. В нашем ассортименте свыше 300 наименований продукции.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- GLOBAL-INFO-END -->

<!-- PRODUCTION -->
<div class="production">
    <div class="container pr-5 pl-5 position-relative">
        <div class="title">
            <h1>Продукция</h1>
        </div>
        <a href="#" class="prev">
            <img src="images/prev.png" alt="">
        </a>
        <a href="#" class="next">
            <img src="images/next.png" alt="">
        </a>
        <div class="owl-carousel owl-carousel-production owl-theme">
            <div class="item">
                <img src="images/production-3.png" alt="">
                <br><br>
                <h6>Стаканчик матовый</h6>
                <p>Для кофе с собой</p>
                <a href="#" class="btn btn-outline-danger btn-global">Посмотреть</a>
            </div>
            <div class="item">
                <img src="images/production-2.png" alt="">
                <br><br>
                <h6>Стаканчик Куба</h6>
                <p>Для кофе с собой</p>
                <a href="#" class="btn btn-outline-danger btn-global">Посмотреть</a>
            </div>
            <div class="item">
                <img src="images/production-3.png" alt="">
                <br><br>
                <h6>Стаканчик Мир</h6>
                <p>Для кофе с собой</p>
                <a href="#" class="btn btn-outline-danger btn-global">Посмотреть</a>
            </div>
            <div class="item">
                <img src="images/production-4.png" alt="">
                <br><br>
                <h6>Стаканчик Драйв</h6>
                <p>Для кофе с собой</p>
                <a href="#" class="btn btn-outline-danger btn-global">Посмотреть</a>
            </div>
        </div>
    </div>
</div>
<!-- PRODUCTION-END -->

<!-- CATEGORY -->
<div class="container">
    <div class="category">
        <div class="title">
            <h1>Категории упаковочной продукции</h1>
        </div>
        <div class="row">
            <div class="col-xl-6 col-md-12 col-lg-6 col-12">
                <div class="card mb-3" >
                    <div class="row no-gutters  ">
                        <div class="col-md-4">
                            <img src="images/category-1.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Для тортов</h5>
                                <p class="card-text">Упаковка для тортов</p>
                                <br>
                                <a href="#" class="btn btn-outline-danger btn-global">Перейти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-lg-6 col-12">
                <div class="card mb-3" >
                    <div class="row no-gutters  ">
                        <div class="col-md-4">
                            <img src="images/category-2.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <p class="card-title">Для салатов</p>
                                <p class="card-text">Сохранность салата дело упаковки</p>
                                <br>
                                <a href="#" class="btn btn-outline-danger btn-global">Перейти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-lg-6 col-12">
                <div class="card mb-3" >
                    <div class="row no-gutters  ">
                        <div class="col-md-4">
                            <img src="images/category-3.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <p class="card-title">Для полуфабрикатов</p>
                                <p class="card-text">Полуфабрикаты хранятся лучше с нашей упаковкой</p>
                                <br>
                                <a href="#" class="btn btn-outline-danger btn-global">Перейти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-lg-6 col-12">
                <div class="card mb-3" >
                    <div class="row no-gutters  ">
                        <div class="col-md-4">
                            <img src="images/category-4.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <p class="card-title">Для суши</p>
                                <p class="card-text">Аппетит нагуливается не только голодом, но и внешним видом</p>
                                <br>
                                <a href="#" class="btn btn-outline-danger btn-global">Перейти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-lg-6 col-12">
                <div class="card mb-3" >
                    <div class="row no-gutters  ">
                        <div class="col-md-4">
                            <img src="images/category-5.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <p class="card-title">Для кондитерских изделий</p>
                                <p class="card-text">Упаковка для кондитерских изделий от Global Pack KZ</p>
                                <br>
                                <a href="#" class="btn btn-outline-danger btn-global">Перейти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-lg-6 col-12">
                <div class="card mb-3" >
                    <div class="row no-gutters  ">
                        <div class="col-md-4">
                            <img src="images/category-6.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <p class="card-title">Для фаст-фуда</p>
                                <p class="card-text">Упаковочная продукция для фаст-фуда.</p>
                                <br>
                                <a href="#" class="btn btn-outline-danger btn-global">Перейти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CATEGORY-END -->

    <!-- GOODS -->
    <div class="goods">
        <div class="title">
            <h1>Что еще можно купить у нас</h1>
        </div>
        <div class="owl-carousel owl-carousel-goods owl-theme">
            <div class="item">
                <a href="#">
                    <img src="images/goods-1.png" alt="">
                    <br>
                    <h5>Упаковка РК-15</h5>
                    <p>Для кондитерских изделий</p>
                </a>
            </div>
            <div class="item">
                <a href="#">
                    <img src="images/goods-2.png" alt="">
                    <br>
                    <h5>Упаковка РКС-500</h5>
                    <p>Для салатов</p>
                </a>
            </div>
            <div class="item">
                <a href="#">
                    <img src="images/goods-1.png" alt="">
                    <br>
                    <h5>Упаковка РК- 21</h5>
                    <p>Для кондитерских изделий</p>
                </a>
            </div>
            <div class="item">
                <a href="#">
                    <img src="images/goods-4.png" alt="">
                    <br>
                    <h5>Упаковка Т – 235</h5>
                    <p>Для тортов</p>
                </a>
            </div>
        </div>
    </div>
    <!-- GOODS-END -->
</div>

<div class="back-call">
    <div class="container">
        <h1><span class="red-text">Есть вопросы?</span>Оставьте заявку</h1>
        <p>Мы ответим на все интуресующие вопросы!</p>
        <div class="row align-items-end">
            <div class="col-xl-4">
                <form action="">
                    <label for="">Введите ваше имя</label>
                    <input type="text" placeholder="Александр">
                </form>
            </div>
            <div class="col-xl-5">
                <form action="">
                    <label for="">Введите вашу почту</label>
                    <input type="email" placeholder="info@mail.com">
                </form>
            </div>
            <div class="col-xl-3">
                <a href="#" class="btn btn-outline-danger btn-global">Оставить заявку</a>
            </div>
        </div>
    </div>
</div>
<!-- ABOUT-COMPANY -->
<div class="container">
    <div class="about-company">
        <div class="title">
            <h1>О компании</h1>
        </div>
        <h4>GLOBAL PACK KZ — производство упаковочной продукции</h4>
        <br>
        <p>Упаковка — это один из самых популярных товаров мира. Почему? Потому что любому товару нужна упаковка. Еда, предметы интерьера, посуда, мебель, украшения, платья — этому всему нужна упаковка, от оберточной бумаги до пакета.
            <br><br>
            Особенно она требуется производителям или торговым точкам, которые реализуют товар прямо в руки клиенту.
            <br><br>
            Очень важно, чтобы упаковка была поистине надежной и не подводила в ответственный момент. Почему стоит сотрудничать с производителями упаковки?
        </p>
        <br>
        <h6>Есть 5 убедительных причин:</h6>
        <br>
        <ul class="p-0">
            <li><img src="images/plus-button.png" alt="">Производитель упаковки — самое компетентное звено. Он знает сырье, из которого производится упаковка.</li>
            <li><img src="images/plus-button.png" alt="">У производителей есть возможность сделать для вас индивидуальный заказ.</li>
            <li><img src="images/plus-button.png" alt="">Возможность заказать любые объемы упаковки.</li>
            <li><img src="images/plus-button.png" alt="">Гарантия качества и изготовление по ГОСТ.</li>
            <li><img src="images/plus-button.png" alt="">Предоставление всех необходимых сертификатов и документов.</li>
        </ul>
    </div>
</div>
<!-- ABOUT-COMPANY-END -->

<div class="end-slide">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/foot-slide.png" class="d-block w-100" alt="...">
                <div class="carousel-text">
                    <h1>Упаковочная продукция</h1>
                    <br>
                    <h5>GLOBAL PACK KZ — универсальный поставщик
                        упаковочной продукции</h5>
                    <br>
                    <div class="end-slide-btn">
                        <a href="#" class="btn btn-outline-danger btn-global">Перейти в каталог</a>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img src="images/foot-slide.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/foot-slide.png" class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"><img src="images/prev-foot.png" alt=""></span>

        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"><img src="images/next-foot.png" alt=""></span>
        </a>
    </div>
</div>

<!-- TITLE-INSTAGRAM -->
<div class="title-inst">
    <img src="images/logo-inst.png" alt="">
    <h2>Мы в инстаграм</h2>
</div>

<!-- TITLE-INSTAGRAM-END-->









<?php include 'footer.php'; ?>