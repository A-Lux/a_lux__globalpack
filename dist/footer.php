    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xl-3">
                    <div class="logo">
                        <img src="images/header-logo.png" alt="">
                    </div>
                </div>
                <div class="col-xl-2 col-6">
                    <a href="#">Продукция</a>
                    <br>
                    <a href="#">О компании</a>
                </div>
                <div class="col-xl-3 col-6">
                    <a href="#">Сертификаты</a>
                    <br>
                    <a href="#">Контакты</a>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="row justify-content-between footer-btn">
                        <a href="#" class="btn btn-outline-danger btn-global">Мы в инстаграм<img src="images/footer-inst.png" alt=""></a>
                        <a href="#" class="btn btn-outline-danger btn-global">Обратный звонок</a>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="footer-loc">
                <div class="row">
                    <div class="col-xl-3 col-md-4 col-12">
                        <h5>Алматы</h5>
                        <p><img src="images/loc-foot.png" alt="">Пр. Рыскулова, 61е</p>
                    </div>
                    <div class="col-xl-2 col-md-4 col-12">
                        <h5>Нур-Султан </h5>
                        <p><img src="images/loc-foot.png" alt="">Пр. Абая, 97</p>
                    </div>
                    <div class="col-xl-3 col-md-4 col-12">
                        <h5>Тараз</h5>
                        <p><img src="images/loc-foot.png" alt="">Мамбет Батыра, 14</p>
                    </div>
                    <div class="col-xl-2 col-md-4 col-12">
                        <h5>Шымкент</h5>
                        <p><img src="images/loc-foot.png" alt="">Ул. Толе би 26</p>
                    </div>
                    <div class="col-xl-2 col-md-6 col-12">
                        <h5>+7 700 123‑60-56</h5>
                        <p><img src="images/wp-foot.png" alt="">Обратный звонок</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="end">
        <div class="container">
            <div class="row justify-content-between">
                <p>Global Pack — все права защищены</p>
                <a href="#">Карта сайта</a>
            </div>
        </div>
    </div>











    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script src="js/main.js"></script>
    </body>

    </html>