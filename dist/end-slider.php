<div class="end-slide">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/foot-slide.png" class="d-block w-100" alt="...">
                <div class="carousel-text">
                    <h1>Упаковочная продукция</h1>
                    <br>
                    <h5>GLOBAL PACK KZ — универсальный поставщик
                        упаковочной продукции</h5>
                    <br>
                    <div class="end-slide-btn">
                        <a href="#" class="btn btn-outline-danger btn-global">Перейти в каталог</a>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img src="images/foot-slide.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="images/foot-slide.png" class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"><img src="images/prev-foot.png" alt=""></span>

        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"><img src="images/next-foot.png" alt=""></span>
        </a>
    </div>
</div>

<div class="title-inst">
    <img src="images/logo-inst.png" alt="">
    <h2>Мы в инстаграм</h2>
</div>
