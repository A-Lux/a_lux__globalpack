<?php include 'header.php'; ?>
<div class="container">
    <div class="page">
        <ul class="p-0 m-0">
            <li><a href="#">Главная</a></li>
            <li><img src="../src/images/page-next.png" alt=""></li>
            <li><a href="#">О нас</a></li>
        </ul>
    </div>
    <div class="title">
        <h1>Контакты</h1>
    </div>
    <div class="row">
        <div class="col-xl-4 col-12">
            <div class="contact">
                <img src="../src/images/contact-loc.png" alt="">
                <div class="contact-text">
                    <h5>Адрес</h5>
                    <p>Щербинка,
                        <br>
                        ул. Железнодорожная 51 А</p>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-12">
            <div class="contact">
                <img src="../src/images/telephone.png" alt="">
                <div class="contact-text">
                    <h5>Телефон</h5>
                    <p>+7 (727) 295-10-00
                        <br>
                        8 961 595 10 10</p>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-12">
            <div class="contact">
                <img src="../src/images/email.png" alt="">
                <div class="contact-text">
                    <h5>Электронная почта</h5>
                    <a href="#">info@mail.ru</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- map -->
<div class="map">
    <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/9429940000833020/center/77.01143503189088,43.34687148390678/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a>
    <div class="dg-widget-link"><a href="http://2gis.kz/almaty/firm/9429940000833020/photos/9429940000833020/center/77.01143503189088,43.34687148390678/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a></div>
    <div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/77.01144,43.346562/zoom/16/routeTab/rsType/bus/to/77.01144,43.346562╎Алматы, международный аэропорт?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Алматы, международный аэропорт</a></div>
    <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
    <script charset="utf-8">
        new DGWidgetLoader({
            "width": '100%',
            "height": 600,
            "borderColor": "#a3a3a3",
            "pos": {
                "lat": 43.34687148390678,
                "lon": 77.01143503189088,
                "zoom": 16
            },
            "opt": {
                "city": "almaty"
            },
            "org": [{
                "id": "9429940000833020"
            }]
        });
    </script><noscript style="color:#c00;font-size:16px;font-weight:bold;"></noscript>
    <div class="your-question">
        <div class="row align-items-end">
            <div class="col-xl-3 col-12">
                <h3>Пишите нам вопросы!</h3>
            </div>
            <div class="col-xl-3 col-12 pl-0">
                <label for="">Ваш номер телефона</label>
                <br>
                <input type="text" placeholder="+7()" id="phone">
            </div>
            <div class="col-xl-3 col-12 p-0">
                <label for="">Ваш вопрос</label>
                <br>
                <input type="text" placeholder="Какие сертификаты у вас?">
            </div>
            <div class="col-xl-3 col-12">
                <a href="#" class="btn btn-outline-danger btn-global">Отправить</a>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>