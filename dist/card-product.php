<?php include 'header.php'; ?>
<?php include 'shoping-fix.php'; ?>
<div class="container">
    <div class="page">
        <ul class="p-0 m-0">
            <li><a href="#">Главная</a></li>
            <li><img src="images/page-next.png" alt=""></li>
            <li><a href="#">О нас</a></li>
        </ul>
    </div>
    <div class="title">
        <h1>Каталог</h1>
    </div>

    <div class="card-product">
        <div class="row">
            <div class="col-xl-6">
                <div class="modal-slider">
                    <div class="owl-carousel owl-carousel-modal owl-theme">
                        <div class="item galery-main">
                            <img src="images/modal-owl.png" alt="">
                        </div>
                        <div class="item galery-main">
                            <img src="images/modal-owl.png" alt="">
                        </div>
                        <div class="item galery-main">
                            <img src="images/modal-owl.png" alt="">
                        </div>
                        <div class="item galery-main">
                            <img src="images/modal-owl.png" alt="">
                        </div>
                    </div>
                    <a href="#" class="galery-prev"><img src="images/galery-prev.png" alt=""></a>
                    <a href="#" class="galery-next"><img src="images/galery-next.png" alt=""></a>
                </div>

                <div class="modal-galery">
                    <div class="owl-carousel owl-carousel-galery owl-theme">
                        <div class="item item-galery">
                            <img src="images/modal-galery-1.png" alt="" data-galery="galery-item-1">
                        </div>
                        <div class="item item-galery">
                            <img src="images/modal-galery-2.png" alt="" data-galery="galery-item-2">
                        </div>
                        <div class="item item-galery">
                            <img src="images/modal-galery-3.png" alt="" data-galery="galery-item-3">
                        </div>
                        <div class="item item-galery">
                            <img src="images/modal-galery-4.png" alt="" data-galery="galery-item-4">
                        </div>
                    </div>
                </div>
                <br><br>
                <h4>Что входит в комплект</h4>
                <div class="modal-galery">
                    <div class="owl-carousel owl-carousel-galery owl-theme">
                        <div class="item-cont">
                            <img src="images/cont.png" alt="">
                            <p>Крышка
                                к контейнеру</p>
                        </div>
                        <div class="item-cont">
                            <img src="images/cont.png" alt="">
                            <p>Крышка
                                к контейнеру</p>
                        </div>
                        <div class="item-cont">
                            <img src="images/cont.png" alt="">
                            <p>Крышка
                                к контейнеру</p>
                        </div>
                        <div class="item-cont">
                            <img src="images/cont.png" alt="">
                            <p>Крышка
                                к контейнеру</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="right-modal-content">
                    <div class="small">Артикул: 4526587</div>
                    <h4>Стакан 100 мл. (120 мл. до края),с нанесением (бум, formacia)</h4>
                    <br>
                    <div class="modal-tab">
                        <ul class="p-0 d-flex tab-head">
                            <li class="tab-active" data-tab="modal-tab-1">Описание</li>
                            <li data-tab="modal-tab-2" data-tab="modal-tab-2">Характеристики</li>
                        </ul>
                        <br>
                        <div class="modal-description description-active" id="modal-tab-1">
                            <p>Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности требуют от нас анализа модели развития. С другой стороны постоянный </p>
                        </div>
                        <div class="modal-description" id="modal-tab-2">
                            <ul class="p-0">
                                <li>Материал изготовления:<span class="font-weight-bold">Полипропилен</span></li>
                                <li>Подходит для горячих продуктов:<span class="font-weight-bold">да</span></li>
                                <li>Диаметр крышки (мм):<span class="font-weight-bold">130</span></li>
                                <li>Новинка:<span class="font-weight-bold">да</span></li>
                                <li>Цвет:<span class="font-weight-bold">Прозрачный</span></li>
                                <li>Производитель:<span class="font-weight-bold">СтиролПласт</span></li>
                                <li>Товары для:<span class="font-weight-bold">супов, горячих блюд</span></li>
                            </ul>
                        </div>
                    </div>
                    <br><br>
                    <div class="product-price">
                        <h4>400 тенге</h4>
                    </div>
                    <br>
                    <div class="quantity">
                        <p>Количество</p>
                        <form action="">
                            <span class="plus"><img src="images/plus.png" alt=""></span>
                            <input type="text" value="500" id="quantity" disabled>
                            <span class="minus"><img src="images/minus.png" alt=""></span>
                        </form>
                        <b>шт.</b>
                    </div>
                    <br>
                    <br>
                    <div class="btn-card-product">
                        <a href="#" class="btn btn-outline-danger btn-basket"><img src="images/shopping-bag.png" alt=""> В корзину</a>
                        <button class="btn btn-outline-secondary btn-global">Купить в 1 клик</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>










<?php include 'footer.php'; ?>